
const messages=require('../messages')
const db=require('../models')
const {Sequelize,Datatypes,Op,QueryTypes}=require('sequelize')
// const { QueryTypes,Sequelize } = require('sequelize');
// const sequelize=new Sequelize( dbConfig.database,
//     dbConfig.username,
//     dbConfig.password,{host:dbConfig.host,
//         dialect:dbConfig.dialect,
//         operatorsAliases:false})
const Review=db.reviews
const  addReview=async(req,res)=>{
    
    let info={
        nRating:req.body.rating,
        sDescription:req.body.description,
    }
    const review= await Review.create(info)
    return res.status(messages.statusCodes.statusSuccess).json(review)
}

const joinProductReview=async(req,res)=>{
   const joinreview=  await db.sequelize.query('SELECT * FROM Reviews JOIN Products ON Reviews.id = Products.id',
    {type: QueryTypes.SELECT})
return  res.status(messages.statusCodes.statusSuccess).json(joinreview)

}

module.exports={
    addReview,
    joinProductReview
}