const product=require('../controllers/productController')
const router=require('express').Router()

router.get('/getAllProducts',product.getAllProducts)
router.get('/getOneProduct:id',product.getOneProduct)
router.get('/publishedProducts',product.publishedProduct)
router.get('/replacementOption',product.replacementOption)
router.get('/clsDemo',product.clsDemo)

router.post('/hook',product.hook)
router.post('/addProduct',product.addProduct)

router.post('/transactionManaged',product.Transaction_sequelize)
router.post('/transactionUnmanaged',product.transactionDemo)
router.get('/readcommited',product.Transaction_readCommited_sequelize)
router.put('/repeatableread',product.repetableRead)


router.get('/allThroughSelect',product.getAllUsersThroughJoin)

router.put('/:id',product.updateProduct)
router.delete('/:id',product.deleteProduct)

module.exports=router