const messages = require("./messages");
const statusCodes = require("./statuscodes");
module.exports = { messages, statusCodes };
