// const db= require('../models/index')
//const Review=db.reviews
// const Review=require('./reviewModel')

const { options } = require('nodemon/lib/config')
const { DataTypes}=require('sequelize')
module.exports=(sequelize)=>{
    const Product=sequelize.define('product',{
        sTitle:{
        type:DataTypes.STRING,  
        allowNull:false,
        unique:true
    
    },

        nPrice:{type:DataTypes.FLOAT,},
        sDescription:{type:DataTypes.TEXT,},
        sPublished:{type:DataTypes.BOOLEAN,},})
          
    // },{hooks:{
    
    //           beforeValidate:(product ,options)=>{
    //               product.sTitle='dummy test data'
    //               console.log('Hooks called')
    //           },//first way
    //           afterValidate:(product ,options)=>{
    //               product.sTitle='Niharika 101'
    //               console.log('Hooks called')
    //           }
    //       },
  
    //         }    )
    //         //2nd way
    //         Product.addHook('afterValidate','2nd hook',(product,options)=>{product.sTitle='Niharika 102' 
    //         console.log('hook2 called')})

    //         Product.beforeValidate('3rdHook',(product,options)=>{product.sTitle='Niharika 103' 
    //         console.log('hook3 called')})
    //         //3 ways to insert hook
    //         Product.removeHook('afterValidate','2nd Hook')//()=>{console.log('hook removed')})
 
   // Product.hasMany(Review,{foreignKey:'id'})
    return Product
}