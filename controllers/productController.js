const db= require('../models/index')
const messages=require('../messages')
const {Sequelize,Datatypes,Op,QueryTypes,Transaction}=require('sequelize')
const cls=require('cls-hooked')

const { sequelize } = require('../models/index')
//min model
const Product=db.products


const Review=db.reviews


//create Product

const  addProduct=async(req,res)=>{
    const message=messages.messages.productadded
    let info={
        sTitle:req.body.title,
        nPrice:req.body.price,
        sDescription:req.body.description,
        sPublished:req.body.published
    }

    const product= await Product.create(info)
    return res.status(messages.statusCodes.statusSuccess).json({product,message})
}

//get products

const  getAllProducts=async(req,res)=>{
   let products= await Product.findAll({
    include:Review,   
    where:{id:1}})
    //await Product.findAll({attributes:['sTitle','nPrice']})
    return res.status(messages.statusCodes.statusSuccess).json(products)
}
//get one product

const  getOneProduct=async(req,res)=>{
    let id=req.params.id
   let product= await Product.findOne({where:{id:id}})
    //await Product.findAll({attributes:['sTitle','nPrice']})
    return res.status(messages.statusCodes.statusSuccess).json(product)
}

//update product


const  updateProduct=async(req,res)=>{
    let id=req.params.id
   let updatedProduct= await Product.update(req.body,{where:{id:id}})
    //await Product.findAll({attributes:['sTitle','nPrice']})
    return res.status(messages.statusCodes.statusSuccess).json(updatedProduct)
}

const  deleteProduct=async(req,res)=>{
    let id=req.params.id
   let deletedProduct= await Product.destroy({where:{id:id}})
    //await Product.findAll({attributes:['sTitle','nPrice']})
    return res.status(messages.statusCodes.statusSuccess).json(deletedProduct)
}
//get published product
const publishedProduct=async(req,res)=>{
     let products= await Product.findAll({where:{sPublished:true}},{order:['sTitle','DESC']},{limit:2})
    return res.status(messages.statusCodes.statusSuccess).json(products)
}
const hook=async(req,res)=>{
  let info={
        sTitle:req.body.title,
        nPrice:req.body.price,
        sDescription:req.body.description,
        sPublished:req.body.published
    }
    const product= await Product.create(info)
    return res.status(200).json(product)
}

const getAllUsersThroughJoin= async(req,res)=>{
     const allProducts=  await db.sequelize.query('SELECT * FROM Products',
    {type: QueryTypes.SELECT,
    model:Product
    })

return  res.status(messages.statusCodes.statusSuccess).json(allProducts)}

const  replacementOption= async(req,res)=>{
     const allProducts=  await db.sequelize.query('SELECT * FROM Products where sTitle=:sTitle',
    {type: QueryTypes.SELECT,
    model:Product,//raw:true,mapToModel:true
    replacements:{sTitle:'Niharika 103'}
    })
    
return  res.status(messages.statusCodes.statusSuccess).json(allProducts)
}

const Transaction_sequelize = async(req, res) => {

    
    let  t  = await db.sequelize.transaction({
        isolationLevel:Transaction.ISOLATION_LEVELS.READ_COMMITTED
    })
    try {
        const product1 = await Product.create({
        sTitle: req.body.title,
        nPrice: req.body.price,
        sDescription: req.body.description,
        sPublished: req.body.published ? req.body.published : false
      })
       setTimeout(async() => {
           await t.commit();
      }, 1200);
      
      return res.json( product1 )
    } catch (error) {
     await t.rollback();
     console.log('rollback');
     return res.json({message:error.message})
    }
}

const Transaction_readCommited_sequelize = async(req, res) => {

  
    let  t  = await db.sequelize.transaction({
        isolationLevel:Transaction.ISOLATION_LEVELS.READ_COMMITTED
    })
    try {
        const product1 = await Product.findAll({where:{id:21},transaction :t});
        console.log(product1)
     t.commit()
    //    setTimeout(async() => {
    //        await t.commit();
    //   }, 50000);
     
      return res.json( product1 )
    } catch (error) {
     await t.rollback();
     console.log('rollback');
     return res.json({message:error.message})
    }
}

const transactionDemo= async(req,res)=>{
    const t =await sequelize.transaction()
      try {
        const product1 = await Product.create({
        sTitle: req.body.title,
        nPrice: req.body.price,
        sDescription: req.body.description,
        sPublished: req.body.published ? req.body.published : false
      }, { transaction: t });
      console.log('commit')
      return res.json(product1)}
      catch(e){
          console.log(e)
          t.rollback()
          return res.json(e)
      }

}

const repetableRead=async(req,res)=>{
    const t= await db.sequelize.transaction({isolationLevel:Transaction.ISOLATION_LEVELS.READ_COMMITTED})
    try{
        const product= await Product.update({nPrice:80},{where:{id:21},transaction:t})
           setTimeout(async() => {
           await t.commit();    
      }, 20000);
      return res.json(product)
    }catch(e){
        await t.rollback()
        
        console.log(e)
    return res.json(e)
    }


}

const clsDemo=async(req,res)=>{
    

    const namespace=cls.createNamespace('my-very-own-namespace')
    
Sequelize.useCLS(namespace)
const create= await Product.create({sTitle:'Pen'})
    sequelize.transaction((t1)=>{
        namespace.get('transaction')===t1
        return res.json(create)
    })
}

module.exports={
    addProduct,
    getAllProducts,
    getOneProduct,
    updateProduct,
    deleteProduct,
    publishedProduct,
    hook,
    getAllUsersThroughJoin,
    replacementOption,
    Transaction_sequelize,
    transactionDemo,
    Transaction_readCommited_sequelize,
    repetableRead,
    clsDemo

}
