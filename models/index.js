const dbConfig=require('../config/dbConfig')
const {Sequelize,Datatypes}=require('sequelize')

const sequelize= new Sequelize(
    dbConfig.database,
    dbConfig.username,
    dbConfig.password,
    {
        host:dbConfig.host,
        dialect:dbConfig.dialect,
        operatorsAliases:false//error recovery

})

sequelize.authenticate()
.then(()=>{console.log('connected')})
.catch((err)=>{console.log(err)})

const db={}
db.Sequelize=Sequelize,
db.sequelize=sequelize

db.products=require('./productModel')(sequelize,Datatypes)
db.reviews=require('./reviewModel')(sequelize,Datatypes)


db.reviews.belongsTo(db.products,{foreignKey:'id'})
db.products.hasMany(db.reviews,{foreignKey:'id'})
db.sequelize.sync({force:false})
.then(console.log('Yes we synced '))

module.exports=db