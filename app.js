const express= require('express')
const app=express();
require('dotenv').config()

app.use(express.json())
app.use(express.urlencoded({extended:true}))

//testing api
const routerProduct=require('./routes/productRoute')
const routerReview=require('./routes/reviewRoute')
app.use('/api/products',routerProduct)
app.use('/api/reviews',routerReview)

app.get('/',(req,res)=>{res.send('hello')})

const PORT=process.env.PORT||3000

app.listen(PORT,()=>{console.log('HELLO '+PORT)})   